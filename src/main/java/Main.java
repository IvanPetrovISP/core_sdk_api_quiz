import exercises.*;
import exercises.processes.GetProcesses;
import exercises.processes.ProcessInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {

        generateNumbers(); // 1 generateNumbers
        matrixSum(); // 3 matrixSum
        getProcesses(); // 5 processInfo
        highlightText(); // 7 highlightText
        createInterval(); // 8 createInterval
        getDate(); // 9 getDate
        getWeekday(); // 10 getWeekday
        getSofiaTime(); //11 getSofiaCurrentTime

    }

    private static void highlightText() {
        System.out.println("- HIGHLIGHT TEXT -");
        String htmlText = "<p>This is an <span>example</span> of how the <span color=\"red\">method</span> should work.</p>";
        System.out.println(htmlText);
        System.out.println(HighlightText.highlightText(htmlText));
        System.out.println();
    }

    private static void getProcesses() throws IOException {
        System.out.println("- GET PROCESSES -");
        List<ProcessInfo> processes = GetProcesses.getProcesses(18 * 1024);
        for (ProcessInfo process : processes) {
            System.out.println(process.toString());
        }
        System.out.println();
    }

    private static void getSofiaTime() {
        System.out.println("- GET SOFIA CURRENT TIME -");
        System.out.println(SofiaCurrentTime.getTime());
        System.out.println();
    }

    private static void getWeekday() {
        System.out.println("- GET WEEKDAY -");
        System.out.print("2020 -> ");
        System.out.println(GetWeekday.getWeekday(2020));
        System.out.println();
    }

    private static void getDate() {
        System.out.println("- GET DATE -");
        Date date = new Date();
        System.out.print("Current Date: ");
        System.out.println(date);
        System.out.print("Date 2 years, 5 months and 2 days ahead: ");
        System.out.println(FutureDate.getFutureDate(date));
        System.out.println();
    }

    private static void createInterval() {
        System.out.println("- CREATE INTERVAL -");
        System.out.println("(,) -> " + CreateInterval.createInterval("(,)"));
        System.out.println("(,5) -> " + CreateInterval.createInterval("(,5)"));
        System.out.println("(,5] -> " + CreateInterval.createInterval("(,5]"));
        System.out.println("(4,5) -> " + CreateInterval.createInterval("(4,5)"));
        System.out.println("(4,5] -> " + CreateInterval.createInterval("(4,5]"));
        System.out.println("(4,) -> " + CreateInterval.createInterval("(4,)"));
        System.out.println("[4,5] -> " + CreateInterval.createInterval("[4,5]"));
        System.out.println("[4,5) -> " + CreateInterval.createInterval("[4,5)"));
        System.out.println("[4,) -> " + CreateInterval.createInterval("[4,)"));
        try {
            CreateInterval.createInterval("[, 5)");
        } catch (IllegalArgumentException e) {
            System.out.println("[, 5) -> " + e.getMessage());
        }
        System.out.println();
    }

    private static void generateNumbers() {
        System.out.println("- GENERATE NUMBERS -");
        List<Integer> list = new ArrayList<>();
        list.add(2);
        list.add(3);
        list.add(4);
        System.out.print("[2, 3, 4] -> ");
        List<Integer> newList = GenerateNumbers.generateNumbers(list);
        System.out.println(newList);
        System.out.println();
    }

    private static void matrixSum() {
        System.out.println("- MATRIX SUM -");
        System.out.println("Filling matrix with random numbers from 0 to 100.");
        MatrixSum matrix = new MatrixSum();
        matrix.generateMatrix();
        matrix.printMatrix();
        System.out.print("Sum of matrix numbers -> ");
        System.out.println(matrix.matrixSum());
        System.out.println();
    }
}
