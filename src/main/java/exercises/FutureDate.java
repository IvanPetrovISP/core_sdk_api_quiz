package exercises;

import java.util.Calendar;
import java.util.Date;

public class FutureDate {

    public static Date getFutureDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, 2);
        calendar.add(Calendar.MONTH, 5);
        calendar.add(Calendar.DAY_OF_MONTH, 2);
        return calendar.getTime();
    }
}
