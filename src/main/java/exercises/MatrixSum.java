package exercises;

import java.util.Random;
import java.util.Scanner;

/**
 * The MatrixSum class holds information about the matrix
 */
public class MatrixSum {
    private Scanner scanner;
    private long[][] matrix;
    private long result;

    public MatrixSum() {
        this.matrix = new long[10][10];
        this.result = 0;
        this.scanner = new Scanner(System.in);
    }

    /**
     * Reads numbers until matrix is full, adding each to the total sum as it is.
     */
    public void readMatrix() {
        this.result = 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = scanner.nextLong();
                result += matrix[i][j];
            }
        }

    }

    /**
     * @return the total sum of the numbers inside the matrix.
     */
    public long matrixSum() {
        return this.result;
    }

    /**
     * Additional method that fills the matrix with randomly generated numbers from 1 to 100.
     * Adds each number to the total sum upon reading.
     * Helps with testing.
     */
    public void generateMatrix() {
        this.result = 0;
        Random random = new Random();

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = random.nextInt(100 + 1);
                result += matrix[i][j];
            }
        }
    }

    /**
     * Printing the matrix.
     */
    public void printMatrix() {
        StringBuilder result = new StringBuilder();
        for (long[] longs : matrix) {
            for (long aLong : longs) {
                result.append(aLong).append("\t");
            }
            result.append(System.lineSeparator());
        }
        System.out.println(result.toString());
    }
}
