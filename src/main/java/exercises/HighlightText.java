package exercises;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HighlightText {

    public static String highlightText (String text) {
        String REGEX = "(?:<span(?:[^>]+)?>)(.*?)(?:<\\/span>)";
        Matcher matcher = Pattern.compile(REGEX).matcher(text);
        StringBuilder result = new StringBuilder(text);
        List<int[]> matchers = new ArrayList<>();

        while (matcher.find()) {
            int start = matcher.start(1);
            int end = matcher.end(1);
            matchers.add(new int[]{start, end});
        }
        for (int i = matchers.size()-1; i >= 0; i--) {
            int start = matchers.get(i)[0];
            int end = matchers.get(i)[1];
            String replacement = String.format("<b>%s</b>", result.substring(start, end).replaceAll("&", ""));
            result.replace(start, end, replacement);
        }
        return result.toString();
    }

}
