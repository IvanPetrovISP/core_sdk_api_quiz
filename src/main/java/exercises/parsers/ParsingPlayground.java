package exercises.parsers;

import exercises.User;

import java.io.File;

public class ParsingPlayground {
    private static String USER_JSON_PATH = "src/main/resources/user.json";
    private static String USER_XML_PATH = "src/main/resources/user.xml";

    public static void main(String[] args) {
        File fileJSON = new File(USER_JSON_PATH);
        File fileXML = new File(USER_XML_PATH);

        Marshaller<User> marshallerJSON = new MarshallerJSON();
        User userJSON = marshallerJSON.read(fileJSON);
        marshallerJSON.write(userJSON);

        Marshaller<User> marshallerStAX = new MarshallerXML_StAX();
        User userStAX = marshallerStAX.read(fileXML);
        marshallerStAX.write(userStAX);

        Marshaller<User> marshallerJAXB = new MarshallerXML_JAXB();
        User userJAXB = marshallerJAXB.read(fileXML);
        marshallerJAXB.write(userJAXB);
    }
}
