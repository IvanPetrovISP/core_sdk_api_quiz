package exercises.parsers;

import exercises.User;

import javax.xml.stream.*;
import javax.xml.stream.events.*;
import java.io.*;
import java.lang.reflect.Field;

public class MarshallerXML_StAX implements Marshaller<User> {

    @Override
    public User read(File file) {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLEventReader reader;
        User xmlUser = null;

        try {
            reader = factory.createXMLEventReader(new FileReader(file));

            while (reader.hasNext()) {
                XMLEvent event = null;

                event = reader.nextEvent();
                if (event.isStartElement()) {
                    StartElement start = event.asStartElement();
                    String localPart = start.getName().getLocalPart();

                    if ("user".equalsIgnoreCase(localPart)) {
                        xmlUser = new User();
                    }

                    switch (localPart) {
                        case "name":
                            Characters name = null;
                            name = reader.nextEvent().asCharacters();
                            xmlUser.setName(name.getData());
                            break;
                        case "age":
                            Characters age = null;
                            age = reader.nextEvent().asCharacters();
                            xmlUser.setAge(Integer.parseInt(age.getData()));
                            break;
                        case "details":
                            Characters details = null;
                            details = reader.nextEvent().asCharacters();
                            xmlUser.setDetails(details);
                            break;
                    }
                }
            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }

        return xmlUser;
    }

    @Override
    public File write(User user) {
        String path = "src/main/resources/";
        String name = "userStAX.xml";

        XMLOutputFactory factory = XMLOutputFactory.newInstance();
        XMLStreamWriter writer;

        File file = new File(path + name);

        try {
            writer = factory.createXMLStreamWriter(new FileWriter(file));

            Field[] declaredFields = User.class.getDeclaredFields();

            writer.writeStartDocument();
            writer.writeStartElement(User.class.getSimpleName().toLowerCase());

            for (Field field : declaredFields) {
                writer.writeStartElement(field.getName().toLowerCase());
                switch (field.getName().toLowerCase()) {
                    case "name":
                        writer.writeCharacters(user.getName());
                        break;
                    case "age":
                        writer.writeCharacters(String.valueOf(user.getAge()));
                        break;
                    case "details":
                        writer.writeCharacters(user.getDetails().toString());
                        break;
                }
                writer.writeEndElement();
            }

            writer.writeEndDocument();
            writer.flush();
            writer.close();
        } catch (XMLStreamException | IOException e) {
            e.printStackTrace();
        }
        return file;
    }

}
