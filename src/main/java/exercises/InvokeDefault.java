package exercises;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;

public class InvokeDefault {

    // for testing purposes
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        String name = User.class.getName();
        String method = "getAge";
        invokeMethod(name, method);
    }

    public static void invokeMethod(String className, String methodName) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class<?> cls = Class.forName(className);
        Object dummy = cls.getDeclaredConstructor().newInstance();
        Method toInvoke = null;
        boolean methodFound = false;
        Method[] declaredMethods = cls.getDeclaredMethods();

        for (Method method : declaredMethods) {
            if (methodName.equals(method.getName())) {
                toInvoke = method;
                methodFound = true;
                break;
            } else if ("defaultMethod".equals(method.getName())) {
                toInvoke = method;
            }
        }

        List<Parameter> parameterDefaults = new ArrayList<>();

        if (methodFound) {
            Parameter[] parameters = toInvoke.getParameters();
            for (Parameter parameter : parameters) {
                if (parameter.getType().isInstance(Object.class)) {
                    Method[] parameterMethods = parameter.getType().getDeclaredMethods();
                    for (Method method : parameterMethods) {
                        if ("getDefaultValue".equals(method.getName())) {
                            parameterDefaults.add((Parameter) method.invoke(parameter.getClass().getDeclaredMethod(String.valueOf(method))));
                        } else {
                            parameterDefaults.add(null);
                        }
                    }
                }
            }

        }

        if (methodFound) {
            toInvoke.invoke(dummy, parameterDefaults.toArray());
        } else if (toInvoke != null) {
            toInvoke.invoke(dummy, parameterDefaults.toArray());
        }

    }
}
