package exercises;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement
@XmlType(propOrder = {"name", "age", "details"})
public class User implements Serializable, Cloneable {
    private String name;
    private int age;
    private transient Object details;

    public String getName() {
        return name;
    }

    @XmlElement
    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    @XmlElement
    public void setAge(int age) {
        this.age = age;
    }

    public Object getDetails() {
        return details;
    }

    @XmlElement
    public void setDetails(Object details) {
        this.details = details;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        User cloned = (User) super.clone();
        return cloned;
    }
}
