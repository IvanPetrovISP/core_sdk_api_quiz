package exercises;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class SofiaCurrentTime {

    public static String getTime() {
        ZonedDateTime time = ZonedDateTime.now(ZoneId.of("Europe/Sofia"));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        return time.format(formatter);
    }
}
