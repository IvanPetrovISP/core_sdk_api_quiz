package exercises;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.*;

public class GenerateJarFile {

    public static void generateJarFile(String from, String location, String name) throws IOException {
        String path = String.format("%s/%s.jar", location, name);
        JarOutputStream jarOutputStream = null;

        try {
            File jarFile = new File(path);
            OutputStream outputStream = new FileOutputStream(jarFile);
            jarOutputStream = new JarOutputStream(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        File[] files = new File(from).listFiles();

        int length = 0;
        byte[] buffer = new byte[1024];

        List<File> fileStorage = new ArrayList<>();
        for (File file : files) {
            if (file.isDirectory()) {
                addFiles(fileStorage, file);
            } else {
                fileStorage.add(file);
            }
        }

        for (File file : fileStorage) {
            JarEntry jarEntry = new JarEntry(file.getPath());
            jarOutputStream.putNextEntry(jarEntry);
            InputStream inputStream = new BufferedInputStream(new FileInputStream(file));

            while ((length = inputStream.read(buffer, 0, buffer.length)) != -1) {
                jarOutputStream.write(buffer, 0, length);
            }

            inputStream.close();
            jarOutputStream.closeEntry();
        }

        jarOutputStream.close();

    }

    private static void addFiles(List<File> fileStorage, File directory) {
        File[] files = directory.listFiles();

        if (files == null) {
            return;
        }

        for (File file : files) {
            if (file.isFile()) {
                fileStorage.add(file);
            } else {
                addFiles(fileStorage, file);
            }
        }
    }
}
