package exercises;

import java.util.Calendar;

public class GetWeekday {

    public static String getWeekday(int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        String result = "";
        switch(calendar.get(Calendar.DAY_OF_WEEK)) {
            case 1:
               result = "Sunday";
               break;
            case 2:
                result = "Monday";
                break;
            case 3:
                result = "Tuesday";
                break;
            case 4:
                result = "Wednesday";
                break;
            case 5:
                result = "Thursday";
                break;
            case 6:
                result = "Friday";
                break;
            case 7:
                result = "Saturday";
                break;
        };

        return result;
    }
}
