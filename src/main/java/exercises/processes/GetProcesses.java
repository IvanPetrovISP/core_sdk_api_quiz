package exercises.processes;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class GetProcesses {

    public static List<ProcessInfo> getProcesses(int memory) throws IOException {
        List<ProcessInfo> processes = new ArrayList<>();

        Process taskList = Runtime.getRuntime()
                .exec(String.format("%s\\System32\\tasklist.exe /fo csv /nh /fi \"MEMUSAGE gt %d\"",
                        System.getenv("windir"), memory));

        BufferedReader reader = new BufferedReader(new InputStreamReader(taskList.getInputStream()));

        String info;
        while ((info = reader.readLine()) != null) {
            String[] tokens = info.split(",");

            String pName = tokens[0].replaceAll("\"", "");
            long pID = Long.parseLong(tokens[1].replaceAll("\"", ""));
            long pMemory = Long.parseLong(tokens[4].replaceAll("�| K|\"", ""));

            processes.add(new ProcessInfo(pName, pID, pMemory));
        }

        return processes;
    }
}
