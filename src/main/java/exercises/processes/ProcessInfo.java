package exercises.processes;

public class ProcessInfo {
    private String name;
    private long processID;
    private long memory;

    public ProcessInfo(String name, long processID, long memory) {
        this.name = name;
        this.processID = processID;
        this.memory = memory;
    }

    public String getName() {
        return this.name;
    }

    public long getProcessID() {
        return this.processID;
    }

    public long getMemory() {
        return this.memory;
    }

    @Override
    public String toString() {
        return String.format("Name: %-40sPID: %-15dMemory: %d", getName(), getProcessID(), getMemory());
    }
}
