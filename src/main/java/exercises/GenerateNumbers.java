package exercises;

import java.util.List;

public class GenerateNumbers {
    public static List<Integer> generateNumbers(List<Integer> numbers) {
        int size = numbers.size();

        for (int i = 0; i < size; i++) {
            int current = numbers.get(i);
            numbers.add(current * current);
            numbers.add(current * current * current);
            numbers.add(current * current * current * current);
        }

        return numbers.subList(size, numbers.size());
    }
}
