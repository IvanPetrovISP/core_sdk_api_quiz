package exercises;

public class CreateInterval {

    public static Interval createInterval(String input) {
        final String REGEX = "^(?:(\\()(\\d+(?:.\\d+)?)?|(\\[)(\\d+(?:.\\d+)?)),(?:(\\d+(?:.\\d+)?)?(\\))|(\\d+(?:.\\d+)?)(\\]))$";
        if (input.matches(REGEX)) {
            return new Interval(input);
        } else {
            throw new IllegalArgumentException("Invalid input.");
        }
    }

    private static class Interval {
        private Character openingBracket;
        private Character closingBracket;
        private Double lowerBoundary;
        private Double higherBoundary;

        public Interval(String input) {
            populateFields(input);
        }

        private void populateFields(String input) {
            String[] tokens = input.split(",");

            this.setOpeningBracket(tokens[0].charAt(0));

            if (tokens[0].length() == 1) {
                this.setLowerBoundary(Double.NEGATIVE_INFINITY);
            } else {
                this.setLowerBoundary(Double.parseDouble(tokens[0].substring(1)));
            }

            if (tokens[1].length() == 1) {
                this.setHigherBoundary(Double.POSITIVE_INFINITY);
            } else {
                this.setHigherBoundary(Double.parseDouble(tokens[1].substring(0, tokens[1].length()-1)));
            }

            this.setClosingBracket(tokens[1].charAt(tokens[1].length()-1));
        }

        private void setOpeningBracket(Character openingBracket) {
            this.openingBracket = openingBracket;
        }

        private void setClosingBracket(Character closingBracket) {
            this.closingBracket = closingBracket;
        }

        private void setLowerBoundary(Double lowerBoundary) {
            this.lowerBoundary = lowerBoundary;
        }

        private void setHigherBoundary(Double higherBoundary) {
            this.higherBoundary = higherBoundary;
        }

        @Override
        public String toString() {
            StringBuilder result = new StringBuilder();

            result
                    .append(openingBracket)
                    .append(lowerBoundary)
                    .append(',')
                    .append(higherBoundary)
                    .append(closingBracket);
            return result.toString();
        }
    }
}
